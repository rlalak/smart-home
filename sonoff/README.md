# Configure sonoff modue

### Sonoff pinout
https://tasmota.github.io/docs/devices/Sonoff-Basic/

### Flash new software
Connect usb UART adapter with sonoff module: Rx=>Tx, Tx=>Rx.
Before connect module to the power, ush button to run module in progrmming mode.
After power is connected release button and flash tasmota soft using Tasmota-PyFlasher-1.0.exe or by web tool https://arendst.github.io/Tasmota-firmware/.
BiInary you can download from https://github.com/arendst/Tasmota/releases (tasmota.bin or tasmota.bin.gz)

### Configure WiFi
SSId: Lalak-Home-Sonoff
Password: s*******a
Hostname: sonoff-kuchnia-led-sufit

### Configure Other:
Device Name: kuchnia-led-sufit

### Configure Module
GPIO3 => Switch 1

### Console
To use Rx pin we need to disable serial port: `SerialLog 0`
Change shitch mode to `follow` (0 = off, 1 = on): `SwitchMode1 1`


### PIR HC-SR501
Modify PIR to run it on 3.3V by removing diod and connecting input directly to voltage regulator output.

