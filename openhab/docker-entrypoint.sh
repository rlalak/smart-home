#!/bin/bash

if [ ! -z ${ZIGBEE_COORDINATOR_ADDRESS} ]; then
    echo "Enable zigbee coordinator socket for ${ZIGBEE_COORDINATOR_ADDRESS}..."
    socat -dd pty,link=/dev/ttyzbbridge,raw,user-late=openhab,group-late=dialout tcp:${ZIGBEE_COORDINATOR_ADDRESS} 2>&1 &

fi

exec /entrypoint "$@"
