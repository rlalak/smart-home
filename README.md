# Project smart home

Description below contains information about how to create almost inteligente home :)
After all steps you will be able to controll your devices by voice using google asistent or using 433MHz remote pilot.

All information are available on other webpages but I needed a lot of time to find all neccessary information because I haven't have any knowledge about home automation systems.

Probably this is not the best way and not the most detailed description about how make it work but should help you to understand whole conception.

<br>

## System components

Whole system requires many components what makes it a little bit complicated and we can meet different problems on step.

### Main components:

 *  USB <=> UART converter: https://allegro.pl/listing?string=usb%20uart%20konwerter
 *  sonoff basic (all versios will work r1/r2/r3): https://allegro.pl/listing?string=sonoff%20basic
 *  tasmota firmware: https://github.com/arendst/Tasmota
 *  home automation system OpenHab: https://www.openhab.org/docs/
 *  mqtt broker Mosquitto: https://mosquitto.org/
 *  Rapsberry Pi 4B (or any other server/computer connected to the same network as sonoff)
 *  OpenHab cloud account: https://myopenhab.org/
 *  Google Home application (iOS or android)

 ### Remote controllers:

 *  433Mhz remote pilots: https://allegro.pl/listing?string=433Mhz%20pilot%20sonoff
 *  module sonoff bridge: https://allegro.pl/listing?string=sonoff%20bridge
 *  portisch firmware: https://github.com/Portisch/RF-Bridge-EFM8BB1/wiki

 ### Sensors and dimmers:

 *  PIR movement sensor: https://allegro.pl/listing?string=pir%20hc-sr501
 *  create dimmer for 12v (i.e. for LED strip): transopror + mosfet

<br>

# Step by step
1. [Sonoff + tasmota](#sonoff-tasmota)
    1. [Flash tasmota firmware](#flashing-tasmota)
    1. Connect sonoff basic to your wifi
    1. Update tasmota firmware
1. 
1. ...
1. Connect your favourite device to sonoff basic

**Remote controll by pilot:**
1. Flash tasmota firmware to sonoff bridge
1. Flash portisch firmware to sonoff bridge
1. confugure sonoff bridge
1. 

**Sensors and dimmers:**
1. PIR movement sensor
    1. Sensor modyfication
    1. Connection to sonoff basic
    1. Sonoff basic configuration
1. extend sonoff basic with a dimmer
    1. create miniboard with own dimmer
    1. connect dimmer board to sonoff basic
    1. configure sonoof basic

<br>

## Sonoff + tasmota

Te 2 elementy opiszę razem bo są to najportsze elementy tej układanki. Moduły zakupiłem na allegro od Komputronika w cenie 27 zł (z fakturą). Obecnie najtaniej da się je dostać za ok. 20zł (również na allegro, na aliexpress ceny są podobne).

More info: https://tasmota.github.io/docs/devices/Sonoff-Basic/

### Flashing tasmota

https://arendst.github.io/Tasmota-firmware/

## Uruchomienie OpenHab
Lokalną instancję OpenHab-a najlepiej uruchomić za pomocą dockera, korzystając z pliku docker-compose załączonego do tego projektu.
```
git clone git@gitlab.com:rlalak/smart-home.git
cd smart-home
docker-compose up -d
```

# Hasło brokera MQTT
W celu ustawienia hasła dla brokera mosquitto zaloguj się do jego kontenera: 
`docker ...`
a następnie użyj narzędzia `mosquitto_passwd` zeby wygenerować hash z nowym hasłem `mosquitto_passwd -c pass.tmp mqtt-user`.
zawartość pliku `pass.tmp` skopiuj do pliku w projekcie `mqtt_password_file` nadpisując istniejącą zawartość.
Wykonaj restart `docker-compose up -d --force-recreate`.


Wchodzimy na adres serwera na port 8081, powinniśmy zobaczyć formularza do stworzenia konta administratora. Podajemy jego nazwę oraz dwukrotnie hasło. W kolejnym kroku wybieramy język oraz strefę czasową.
Na następnym ekranie klikamy "Rozpocznij konfigurację" i wskazujemy lokalizajcę naszego domu i klikamy ustaw lokalizację.
W kolejnym kroku wybieramy dodatki:
 - MQTT Binding
 - openHAB Cloud Connector
 - JSONPath Transformation
 i klikamy zainstaluj dodatki.

W zakładce ustawienia w sekcji "Other services" wchodzimy w "MQTT system broker connection" i ustawiamy host (ip naszego serwera), username i password.


OpenHab
https://www.openhab.org/addons/transformations/jsonpath/

SetOption1 1

PulseTime3 700

SetOption51	Enable GPIO9 and GPIO10 
